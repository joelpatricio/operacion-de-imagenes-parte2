package parte2Bloques;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class ImageOperator {
	public BufferedImage img1;
	public BufferedImage img2;
	public BufferedImage imgR;
	public Boolean[][] chunkMatrix;
	public int chunkRows;
	public int chunkCols;
	
	
	
	public ImageOperator() {
		// TODO Auto-generated constructor stub
	}
	public int getChunkRows() {
		return chunkRows;
	}
	public int getChunkCols() {
		return chunkCols;
	}
	public void setChunkRows(int chunkRows) {
		this.chunkRows=chunkRows;
	}
	public void setChunkCols(int chunkCols) {
		this.chunkCols=chunkCols;
	}
	
	
	public void suma(BufferedImage imagen1, BufferedImage imagen2) {
		for(int y=0; y<imagen1.getHeight(); y++) {
			for(int x=0; x<imagen1.getWidth(); x++) {
				int rgb1=imagen1.getRGB(x, y);
				int rgb2=imagen2.getRGB(x, y);
				int rgbR=rgb1+rgb2;
				if(rgbR>255) {
					rgbR=255;
				}
				imagen1.setRGB(x, y, rgbR);
			}						
		}
		exportar(imagen1);
	}
	
	public void resta(BufferedImage imagen1, BufferedImage imagen2) {
		for(int x=0; x<imagen1.getWidth(); x++) {
			for(int y=0; y<imagen1.getHeight(); y++) {
				int rgb1=imagen1.getRGB(x,y);
				int rgb2=imagen2.getRGB(x, y);
				int rgbR=rgb1-rgb2;
				if(rgbR<0) {
					rgbR=0;
				}
				imagen1.setRGB(x, y, rgbR);
			}
		}
		exportar(imagen1);
	}
	
	public void multiplicacion(BufferedImage imagen1, BufferedImage imagen2) {
		for(int x=0; x<imagen1.getWidth(); x++) {
			for(int y=0; y<imagen1.getHeight(); y++) {
				int rgb1=imagen1.getRGB(x,y);
				int rgb2=imagen2.getRGB(x, y);
				int rgbR=rgb1*rgb2;
				rgbR=rgbR/255;
				imagen1.setRGB(x, y, rgbR);
			}
		}
		exportar(imagen1);
	}
	
	public void combinacionLineal(BufferedImage imagen1, BufferedImage imagen2) {
		for(int x=0; x<imagen1.getWidth(); x++) {
			for(int y=0; y<imagen1.getHeight(); y++) {
				int rgb1=imagen1.getRGB(x,y);
				int rgb2=imagen2.getRGB(x, y);
				int rgbR=(int) ((0.2*rgb1)+(0.8*rgb2));
				imagen1.setRGB(x, y, rgbR);
			}
		}
		exportar(imagen1);
	}
	
	public void exportar(BufferedImage imagen1) {
		try {
			File f=new File("ImagenResultante.jpg");
			ImageIO.write(imagen1,"jpg",f);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}		
	}
	
	public int[][] pixel2matrix(BufferedImage imgX){
		int [][]matrix=new int [imgX.getWidth()][imgX.getHeight()];
		for(int y=0; y<imgX.getHeight(); y++) {
			for(int x=0; x<imgX.getWidth(); x++) {
				matrix[x][y]=imgX.getRGB(x, y);
			}						
		}
		return matrix;
	}
	
	public BufferedImage matrix2pixel(int[][]matrix) {
		BufferedImage imgX = null;
		for(int y=0; y<matrix.length; y++) {
			for(int x=0; x<matrix[y].length; x++) {
				imgX.setRGB(x, y, matrix[x][y]);
			}						
		}
		return imgX;
	}
	
	

}
