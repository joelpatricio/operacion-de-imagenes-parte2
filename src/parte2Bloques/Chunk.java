package parte2Bloques;

public class Chunk {
	public int[][] cpos;
	public int crows;
	public int ccols;
	
	public Chunk() {
		// TODO Auto-generated constructor stub
	}
	public int[][] getCpos(){
		return cpos;
	}
	public int getCrows(){
		return crows;
	}
	public int getCcols(){
		return ccols;
	}
	
	public void setCpos( int[][] cpos){
		this.cpos=cpos;
	}
	public void setCrows( int crows){
		this.crows=crows;
	}
	public void setCcols( int ccols){
		this.ccols=ccols;
	}
	
	public Chunk obtainChunk(int[][]cpos1, int crows1, int ccols1) {
		Chunk obj=new Chunk();
		obj.setCpos(cpos1);
		obj.setCrows(crows1);
		obj.setCcols(ccols1);
		return obj;
	}
	
	public int chunkCounter(int cc, int cr) {
		return cc*cr;
	}
	
	

}
