package OperacionesImagenes;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.wb.swt.SWTResourceManager;

import Operaciones.ImageOperator;


public class interfaz {

	protected Shell shell;
	private final FormToolkit formToolkit = new FormToolkit(Display.getDefault());
	public BufferedImage imagen1=null;
	public BufferedImage imagen2=null;
	public BufferedImage imagenResultante=null;
	ImageOperator operaciones=new ImageOperator();

	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			interfaz window = new interfaz();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void abrirVentana() {
		interfaz2 window2 = new interfaz2();
		window2.open();
	}
	
	public void operaciones(char opc) {
		
		if(opc=='+') {
			//System.out.print("Operacion: "+opc);
			imagenResultante=imagen1;
			operaciones.suma(imagen1, imagen2);
		}
		else if(opc=='-') {
			//System.out.print("Operacion: "+opc);
			operaciones.resta(imagen1, imagen2);
		}
		else if(opc=='*') {
			//System.out.print("Operacion: "+opc);
			operaciones.multiplicacion(imagen1, imagen2);
		}
		else if(opc=='#'){
			//System.out.print("Operacion: "+opc);
			operaciones.combinacionLineal(imagen1, imagen2);
		}
		
		
	}
	
	

	/**
	 * Open the window.
	 * @throws UnsupportedLookAndFeelException 
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 * @throws ClassNotFoundException 
	 */
	public void open() throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException {
		Display display = Display.getDefault();
		createContents();
		shell.open();
		shell.layout();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shell = new Shell();
		shell.setSize(736, 513);
		shell.setText("SWT Application");
		
		Label lblNewLabel = new Label(shell, SWT.BORDER | SWT.CENTER);
		lblNewLabel.setFont(SWTResourceManager.getFont("Segoe UI", 7, SWT.NORMAL));
		lblNewLabel.setImage(SWTResourceManager.getImage(interfaz.class, "/OperacionesImagenes/photo.png"));
		lblNewLabel.setBounds(10, 10, 286, 314);
		formToolkit.adapt(lblNewLabel, true, true);
		
		Label label = new Label(shell, SWT.BORDER | SWT.CENTER);
		label.setImage(SWTResourceManager.getImage(interfaz.class, "/OperacionesImagenes/photo.png"));
		label.setBounds(424, 10, 286, 314);
		formToolkit.adapt(label, true, true);
		
		Combo combo = new Combo(shell, SWT.NONE);
		combo.setFont(SWTResourceManager.getFont("Segoe UI", 12, SWT.BOLD));
		combo.setItems(new String[] {"+", "-", "*", "#"});
		combo.setBounds(329, 148, 75, 23);
		formToolkit.adapt(combo);
		formToolkit.paintBordersFor(combo);
		combo.select(0);
		
		Button btnCargarImagen = new Button(shell, SWT.NONE);
		btnCargarImagen.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				JFileChooser fc=new JFileChooser();
				fc.setDialogTitle("Seleccione una imagen");
				
				if(fc.showOpenDialog(null)==JFileChooser.APPROVE_OPTION) {
					//File archivo1=new File(fc.getSelectedFile().toString());
					lblNewLabel.setImage(SWTResourceManager.getImage(fc.getSelectedFile().toString()));
					String direccion=fc.getSelectedFile().toString();
					try {
						imagen1=ImageIO.read(new File(direccion));
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}
		});
		btnCargarImagen.setBounds(99, 363, 106, 25);
		formToolkit.adapt(btnCargarImagen, true, true);
		btnCargarImagen.setText("Cargar Imagen 1");
		
		Button btnCargarImagen_1 = new Button(shell, SWT.NONE);
		btnCargarImagen_1.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				JFileChooser fc=new JFileChooser();
				fc.setDialogTitle("Seleccione una imagen");
				
				if(fc.showOpenDialog(null)==JFileChooser.APPROVE_OPTION) {
					//File archivo2=new File(fc.getSelectedFile().toString());
					label.setImage(SWTResourceManager.getImage(fc.getSelectedFile().toString()));
					String direccion=fc.getSelectedFile().toString();
					try {
						imagen2=ImageIO.read(new File(direccion));
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}	
				}
			}
		});
		btnCargarImagen_1.setText("Cargar Imagen 2");
		btnCargarImagen_1.setBounds(514, 363, 106, 25);
		formToolkit.adapt(btnCargarImagen_1, true, true);
		
		Button btnOperar = new Button(shell, SWT.NONE);
		btnOperar.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				String seleccion=combo.getText();
				char c[];
				c=seleccion.toCharArray();
				operaciones(c[0]);
				abrirVentana();
			}
		});
		btnOperar.setBounds(329, 416, 75, 25);
		formToolkit.adapt(btnOperar, true, true);
		btnOperar.setText("Operar");
		shell.setTabList(new Control[]{btnCargarImagen, btnCargarImagen_1, combo, btnOperar});

	}
}
